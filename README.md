# Install

```bash
pip install -r requirements.txt
export transmission_ip="192.168.122.58"
```

## Dev

```bash

docker build -t loic_mpt/torrentapi .
docker run --rm -p 8080:8080 -e api_ip="0.0.0.0" loic_mpt/torrentapi:latest 

```
