FROM python:latest

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt
RUN mkdir /templates/
COPY ./iframe.html /templates/iframe.html
COPY ./server.py /server.py
CMD ["flask", "run", "--host", "0.0.0.0", "--port", "80"]