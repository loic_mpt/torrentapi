#!/usr/env/python3
import transmissionrpc
from flask import Flask, request, render_template
from os import getenv

def addTorrent(magnet):
    con = transmissionrpc.Client(getenv("transmission_ip", "192.168.0.101"), port=getenv("transmission_port", 9091))
    torrent = con.add_torrent(magnet)
    torrent.start()

app = Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    return render_template("iframe.html", title = 'Projects')

@app.route('/addTorrent', methods=['GET'])
def appendTorrent():
    try:
        if not "magnet" in request.args:
            raise Exception('pas de magnet')
        print(request.args["magnet"])
        addTorrent(request.args["magnet"])
        return "ok", 200
    except:
        return "il y a une erreur", 500

app.run(host=getenv("api_ip", '0.0.0.0'), port=getenv("api_port", 80))